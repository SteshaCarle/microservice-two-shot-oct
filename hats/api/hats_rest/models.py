from django.db import models

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200)
    closet_name = models.CharField(max_length=100)



class Hats(models.Model):
    name = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200)
    picture_url = models.URLField()

    location = models.ForeignKey(
        "LocationVO",
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    # def get_api_url(self):
    #     return reverse("api_show_attendee", kwargs={"id": self.id})
