from django.shortcuts import render
from django.http import JsonResponse
from .models import Hats, LocationVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json

# Create your views here.

class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "name",
        "fabric",
        "color",
        "picture_url",
        "id",
        ]

@require_http_methods(["GET", "POST", "DELETE"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )
    # else: #DELETE
    #     count, _ = Hats.objects.filter(id=id).DELETE
    #     return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "DELETE"])
def api_hat_details(request, id):
    if request.method == "GET":
        hat = Hats.objects.get(id=id)
        return JsonResponse(
            {"hat": hat},
            encoder=HatListEncoder,
        )
    else:
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
