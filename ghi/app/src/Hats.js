import React, { useEffect, useState } from 'react'

function Hats() {
    const [hats, setHats] = useState([])
    const fetchData = async () => {
        const url = "http://localhost:8090/api/hats/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    }

    const handleDelete = async (id) => {
        try {
            const url = `http://localhost:8090/api/hats/${id}`
            const response = await fetch(url, {
                method:
                'DELETE'
            })
            if (response.ok) {
                fetchData()
            } else {
                console.error("Couldn't delete hat")
            }
        } catch (error) {
            console.error("Couldn't delete hat", error)
        }

    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Color</th>
                    <th>Fabric</th>
                    <th>Photo</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.name }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.fabric }</td>
                            <td>
                                <img src={ hat.picture_url } width="180" height="140" alt="hat_img"/>
                            </td>
                            <td>
                                <button onClick={() => handleDelete(hat.id)}>Delete</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>

    )
}

export default Hats
